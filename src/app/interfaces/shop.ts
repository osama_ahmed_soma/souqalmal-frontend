import { GeometryCoordinates } from "./geometry-coordinates";

export interface Shop {
  id: number;
  name: string;
  picture?: string;
  coordinates: GeometryCoordinates;
}
