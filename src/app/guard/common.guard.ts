import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  CanLoad,
  Router,
  RouterStateSnapshot,
  UrlTree
} from '@angular/router';
import { Observable } from 'rxjs';
import { LoginService } from '../services/login.service';

@Injectable({
  providedIn: 'root'
})
export class CommonGuard implements CanActivate, CanLoad {
  constructor(private router: Router, private loginService: LoginService) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    return this.canLoad();
  }
  canLoad(): Observable<boolean> | Promise<boolean> | boolean {
    if (!this.loginService.isAuthenticated()) {
      this.router.navigate(['/login']);
    }
    return this.loginService.isAuthenticated();
  }
}
