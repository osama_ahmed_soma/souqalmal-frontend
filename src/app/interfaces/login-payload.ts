import { Coordinates } from "./coordinates";
export interface LoginPayload {
  email: string;
  password: string;
  coordinates: Coordinates;
}
