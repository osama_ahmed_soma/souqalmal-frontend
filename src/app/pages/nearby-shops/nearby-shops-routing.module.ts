import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NearByShopsComponent } from './nearby-shops.component';

const routes: Routes = [{ path: '', component: NearByShopsComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NearByShopsRoutingModule {}
