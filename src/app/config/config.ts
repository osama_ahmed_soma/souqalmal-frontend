const backendUrl = 'http://localhost:8080';
export default {
  backend: {
    url: backendUrl,
    api: `${backendUrl}/api`
  }
};
