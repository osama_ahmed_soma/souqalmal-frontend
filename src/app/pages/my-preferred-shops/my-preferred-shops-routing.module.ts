import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MyPreferredShopsComponent } from './my-preferred-shops.component';

const routes: Routes = [{ path: '', component: MyPreferredShopsComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MyPreferredShopsRoutingModule { }
