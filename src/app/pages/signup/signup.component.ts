import { AfterViewInit, Component, ElementRef, OnInit } from "@angular/core";
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators
} from "@angular/forms";
import { Router } from "@angular/router";
import { MustMatch } from "src/app/helpers/mustMatch";
import { LoginService } from "src/app/services/login.service";

@Component({
  selector: "app-signup",
  templateUrl: "./signup.component.html",
  styleUrls: ["./signup.component.scss"]
})
export class SignupComponent implements OnInit, AfterViewInit {
  signupForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private elementRef: ElementRef,
    private loginService: LoginService,
    private router: Router
  ) {}

  ngOnInit() {
    this.signupForm = this.fb.group(
      {
        name: ["", Validators.required],
        email: ["", [Validators.required, Validators.email]],
        password: ["", Validators.required],
        retype_password: ["", Validators.required]
      },
      {
        validator: MustMatch("password", "retype_password")
      }
    );
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.signupForm.controls;
  }

  _generateEmailError(email: AbstractControl): string {
    return email.hasError("required")
      ? "You must enter a value"
      : email.hasError("email")
      ? "Not a valid email"
      : "";
  }
  _generateRequiredError(property: AbstractControl): string {
    return property.hasError("required") ? "You must enter a value" : "";
  }

  getErrorMessage(controlName: string = "email") {
    switch (controlName) {
      case "email":
        return this._generateEmailError(this.f.email);
      case "name":
        return this._generateRequiredError(this.f.name);
      case "password":
        return this._generateRequiredError(this.f.password);
      case "retype_password":
        return this._generateRequiredError(this.f.retype_password);
    }
  }

  ngAfterViewInit(): void {
    this.elementRef.nativeElement.ownerDocument.body.style.backgroundColor =
      "#ddd";
  }

  signup(): void {
    if (this.signupForm.valid) {
      const { name, email, password } = this.f;
      this.loginService
        .signup({
          name: name.value,
          email: email.value,
          password: password.value
        })
        .subscribe(() => {
          this.router.navigateByUrl("/nearby-shops");
        });
    }
  }

  toLogin(): void {
    this.router.navigateByUrl("/login");
  }
}
