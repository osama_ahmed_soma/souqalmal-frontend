import { HttpClient, HttpResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { of } from "rxjs/internal/observable/of";
import { catchError, tap } from "rxjs/operators";
import config from "../config/config";
import { InterceptorSkipHeader } from "../helpers/skipHeaders";
import { LoginPayload } from "../interfaces/login-payload";
import { SingupPayload } from "../interfaces/singup-payload";
import { Token } from "../interfaces/token";
import { JWT_REFRESH_TOKEN, JWT_TOKEN } from "../types";

@Injectable({
  providedIn: "root"
})
export class LoginService {
  url = `${config.backend.api}/user`;

  constructor(private http: HttpClient) {}

  isAuthenticated(): boolean {
    return !!this.getToken();
  }

  getToken() {
    return localStorage.getItem(JWT_TOKEN);
  }

  signup(signupPayload: SingupPayload) {
    return this.http
      .post(`${this.url}`, signupPayload, {
        headers: InterceptorSkipHeader
      })
      .pipe(
        tap<Token>(tokens => this.doLoginUser(tokens)),
        // mapTo(v => v),
        catchError(error => {
          alert(error.error);
          return of(false);
        })
      );
  }
  login(loginPayload: LoginPayload) {
    return this.http
      .post(`${this.url}/login`, loginPayload, {
        headers: InterceptorSkipHeader
      })
      .pipe(
        tap<Token>(tokens => this.doLoginUser(tokens)),
        // mapTo(v => v),
        catchError(error => {
          alert(error.error);
          return of(false);
        })
      );
  }

  refreshToken() {
    return this.http
      .post<HttpResponse<null>>(`${this.url}/refresh`, {
        refreshToken: this.getRefreshToken()
      })
      .pipe(
        tap(response => {
          this.storeToken(response.headers.get("token"));
        })
      );
  }

  private getRefreshToken() {
    return localStorage.getItem(JWT_REFRESH_TOKEN);
  }

  private doLoginUser(tokens: Token) {
    this.storeTokens(tokens);
  }

  private storeTokens({ token, refreshToken }: Token) {
    localStorage.setItem(JWT_TOKEN, token);
    localStorage.setItem(JWT_REFRESH_TOKEN, refreshToken);
  }

  private storeToken(token: string): void {
    localStorage.setItem(JWT_TOKEN, token);
  }
}
