import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { LayoutComponent } from "./layout.component";

const routes: Routes = [
  {
    path: "",
    component: LayoutComponent,
    children: [
      { path: "", redirectTo: "nearby-shops", pathMatch: "prefix" },
      {
        path: "nearby-shops",
        loadChildren: () =>
          import("../../pages/nearby-shops/nearby-shops.module").then(
            m => m.NearByShopsModule
          )
      },
      {
        path: "my-preferred-shops",
        loadChildren: () =>
          import(
            "../../pages/my-preferred-shops/my-preferred-shops.module"
          ).then(m => m.MyPreferredShopsModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LayoutRoutingModule {}
