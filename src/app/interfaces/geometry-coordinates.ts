export interface GeometryCoordinates {
  type: string;
  coordinates: number[];
}
