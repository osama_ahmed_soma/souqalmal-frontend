import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs/internal/Observable";
import config from "../config/config";
import { Shop } from "../interfaces/shop";

@Injectable({
  providedIn: "root"
})
export class ShopService {
  url = `${config.backend.api}/shop`;

  constructor(private http: HttpClient) {}

  listNearBy(): Observable<Shop[]> {
    return this.http.get<Shop[]>(`${this.url}/nearBy`);
  }

  listPreferred(): Observable<Shop[]> {
    return this.http.get<Shop[]>(`${this.url}/preferred`);
  }

  like(shopID: number) {
    return this.http.post(`${this.url}/like`, {
      shopID
    });
  }

  disLike(shopID: number) {
    return this.http.post(`${this.url}/disLike`, {
      shopID
    });
  }

  remove(shopID: number) {
    return this.http.post(`${this.url}/remove`, {
      shopID
    });
  }
}
