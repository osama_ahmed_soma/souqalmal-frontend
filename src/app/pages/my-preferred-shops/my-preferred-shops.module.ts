import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { MatButtonModule } from "@angular/material/button";
import { MatCardModule } from "@angular/material/card";
import { MyPreferredShopsRoutingModule } from "./my-preferred-shops-routing.module";
import { MyPreferredShopsComponent } from "./my-preferred-shops.component";

@NgModule({
  declarations: [MyPreferredShopsComponent],
  imports: [
    CommonModule,
    MyPreferredShopsRoutingModule,
    MatCardModule,
    MatButtonModule
  ]
})
export class MyPreferredShopsModule {}
