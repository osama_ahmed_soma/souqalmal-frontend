import { Component, OnInit } from "@angular/core";
import { Shop } from "src/app/interfaces/shop";
import { ShopService } from "src/app/services/shop.service";

@Component({
  selector: "app-my-preferred-shops",
  templateUrl: "./my-preferred-shops.component.html",
  styleUrls: ["./my-preferred-shops.component.scss"]
})
export class MyPreferredShopsComponent implements OnInit {
  shops: Shop[] = [];

  constructor(private shopService: ShopService) {}

  ngOnInit() {
    this.shopService.listPreferred().subscribe(
      shops => {
        this.shops = shops;
      },
      err => console.log(err)
    );
  }

  remove(shopID: number): void {
    this.shopService.remove(shopID).subscribe(() => {
      this.shops = this.shops.filter(({ id }) => id !== shopID);
    });
  }
}
