import { AfterViewInit, Component, ElementRef, OnInit } from "@angular/core";
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators
} from "@angular/forms";
import { Router } from "@angular/router";
import { LoginService } from "src/app/services/login.service";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"]
})
export class LoginComponent implements OnInit, AfterViewInit {
  loginForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private elementRef: ElementRef,
    private loginService: LoginService,
    private router: Router
  ) {}

  ngOnInit() {
    this.loginForm = this.fb.group({
      email: ["", [Validators.required, Validators.email]],
      password: ["", Validators.required]
    });
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.loginForm.controls;
  }

  _generateEmailError(email: AbstractControl): string {
    return email.hasError("required")
      ? "You must enter a value"
      : email.hasError("email")
      ? "Not a valid email"
      : "";
  }
  _generateRequiredError(property: AbstractControl): string {
    return property.hasError("required") ? "You must enter a value" : "";
  }

  getErrorMessage(controlName: string = "email") {
    switch (controlName) {
      case "email":
        return this._generateEmailError(this.f.email);
      case "password":
        return this._generateRequiredError(this.f.password);
    }
  }

  ngAfterViewInit(): void {
    this.elementRef.nativeElement.ownerDocument.body.style.backgroundColor =
      "#ddd";
  }

  login(): void {
    if (this.loginForm.valid) {
      const { email, password } = this.f;
      this.loginService
        .login({
          email: email.value,
          password: password.value,
          coordinates: {
            lat: 25.256931,
            lng: 55.326731
          }
        })
        .subscribe(() => {
          this.router.navigateByUrl("/nearby-shops");
        });
    }
  }

  toSignUp(): void {
    this.router.navigateByUrl("/signup");
  }
}
