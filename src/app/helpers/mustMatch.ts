import { FormGroup } from '@angular/forms';

// custom validator to check that two fields match
export function MustMatch(
  controlName: string = 'password',
  matchingControlName: string = 'retype_password'
) {
  return (formGroup: FormGroup) => {
    formGroup.get(controlName);
    const control = formGroup.get(controlName);
    const matchingControl = formGroup.get(matchingControlName);

    if (control.value !== matchingControl.value) {
      matchingControl.setErrors({ mustMatch: true });
    }
  };
}
