import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { MatButtonModule } from "@angular/material/button";
import { MatCardModule } from "@angular/material/card";
import { NearByShopsRoutingModule } from "./nearby-shops-routing.module";
import { NearByShopsComponent } from "./nearby-shops.component";

@NgModule({
  declarations: [NearByShopsComponent],
  imports: [
    CommonModule,
    NearByShopsRoutingModule,
    MatCardModule,
    MatButtonModule
  ]
})
export class NearByShopsModule {}
