import { Component, OnInit } from "@angular/core";
import { Shop } from "src/app/interfaces/shop";
import { ShopService } from "src/app/services/shop.service";

// interface ShopWithDistanceInterface extends Shop {
//   distance: number;
// }

@Component({
  selector: "app-nearby-shops",
  templateUrl: "./nearby-shops.component.html",
  styleUrls: ["./nearby-shops.component.scss"]
})
export class NearByShopsComponent implements OnInit {
  shops: Shop[] = [];

  constructor(private shopService: ShopService) {}

  ngOnInit() {
    this.shopService.listNearBy().subscribe(
      shops => {
        this.shops = shops;
      },
      err => console.log(err)
    );
  }

  private removeShopByID(shopID: number): void {
    this.shops = this.shops.filter(({ id }) => id !== shopID);
  }

  like(shopID: number): void {
    this.shopService.like(shopID).subscribe(() => {
      this.removeShopByID(shopID);
    });
  }

  disLike(shopID: number): void {
    // remove for 2 hours
    this.shopService.disLike(shopID).subscribe(() => {
      this.removeShopByID(shopID);
    });
  }
}
